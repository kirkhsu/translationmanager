package com.kirkhsu.sample;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;

public class MongoDBSample {
	private MongoClient mongoClient = null;
	
	public MongoDBSample(){
		if(mongoClient == null) {
			mongoClient = MongoClients.create("mongodb://localhost");
		}
	}
	
	public void testConnection() {
		MongoDatabase database = mongoClient.getDatabase("test");
		System.out.println("database = " + database);
	}

	
}

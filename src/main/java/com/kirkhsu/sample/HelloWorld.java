package com.kirkhsu.sample;

public class HelloWorld {
	public static void main(String[] args) {
		System.out.println("Hello world !");
		
		java.util.logging.Logger.getLogger("org.mongodb.driver").setLevel(java.util.logging.Level.SEVERE);
		
		MongoDBSample mongoSample = new MongoDBSample();
		mongoSample.testConnection();
	}
}
